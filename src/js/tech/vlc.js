/**
 * @file vlc.js
 * VideoJS-SWF - Custom Vlc Player with HTML5-ish API
 * https://github.com/zencoder/video-js-swf
 * Not using setupTriggers. Using global onEvent func to distribute events
 */

import Tech from './tech';
import * as Dom from '../utils/dom.js';
import * as Url from '../utils/url.js';
import { createTimeRange } from '../utils/time-ranges.js';
import Component from '../component';
import window from 'global/window';
import assign from 'object.assign';

let navigator = window.navigator;
/**
 * Vlc Media Controller - Wrapper for fallback SWF API
 *
 * @param {Object=} options Object of option names and values
 * @param {Function=} ready Ready callback function
 * @extends Tech
 * @class Vlc
 */
class Vlc extends Tech {

  constructor(options, ready){
    super(options, ready);

    // Set the source when ready
    if (options.source) {
      this.ready(function(){
        this.setSource(options.source);
      }, true);
    }

    // Having issues with Vlc reloading on certain page actions (hide/resize/fullscreen) in certain browsers
    // This allows resetting the playhead when we catch the reload
    if (options.startTime) {
      this.ready(function(){
        this.load();
        this.play();
        this.currentTime(options.startTime);
      }, true);
    }

    // Add global window functions that the swf expects
    // A 4.x workflow we weren't able to solve for in 5.0
    // because of the need to hard code these functions
    // into the swf for security reasons
    window.videojs = window.videojs || {};
    window.videojs.Vlc = window.videojs.Vlc || {};
    // window.videojs.Vlc.onReady = Vlc.onReady;
    // window.videojs.Vlc.onEvent = Vlc.onEvent;
    // window.videojs.Vlc.onError = Vlc.onError;

    this.on('seeked', function() {
      this.lastSeekTarget_ = undefined;
    });

    this.on('fullscreenchange', function() {
        // Workaround to force vmem to resize the video
        var pos = this.getApi().input.position;
        this.getApi().playlist.stop();
        this.getApi().playlist.play();
        this.getApi().input.position = pos;
    });

    this.ready(function() {
        this.trigger('loadstart');
    });



    // setTimeout(function() {
    //     Component.triggerReady();
    // }, 100);
  }

  /**
   * Create the component's DOM element
   *
   * @return {Element}
   * @method createEl
   */
  createEl() {
    let options = this.options_;

    // Generate ID for VLC object
    let objId = options.techId + '_vlc_api';

    // Merge default vlcvars with ones passed in to init
    let vlcVars = assign({

      // Player Settings
      // 'target': options.source,
      'autoplay': options.autoplay,
      'preload': options.preload,
      'loop': options.loop,
      'muted': options.muted

    }, options.vlcVars);

    // Merge default parames with ones passed in
    let params = assign({
      'wmode': 'opaque', // Opaque is needed to overlay controls, but can affect playback performance
      'bgcolor': '#000000' // Using bgcolor prevents a white vlc when the object is loading
    }, options.params);

    // Merge default attributes with ones passed in
    let attributes = assign({
      'id': objId,
      'name': objId, // Both ID and Name needed for vlc to identify itself
      'class': 'vjs-tech'
    }, options.attributes);

    this.el_ = Vlc.embed(vlcVars, params, attributes);
    this.el_.tech = this;

    // Add VLC events
    Vlc.registerEvent(this.getApi(), 'MediaPlayerOpening', function() {
      
        this.trigger('loadstart');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerBuffering', function() {
        this.trigger('loadeddata');
        
        // Notify video.js to refresh some data from VLC
        this.trigger('volumechange');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerPlaying', function() {
        this.trigger('play');
        this.trigger('playing');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerPaused', function() {
        this.trigger('pause');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerStopped', function() {
        this.trigger('pause');
        this.trigger('ended');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerEndReached', function() {
        this.trigger('pause');
        this.trigger('ended');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerTimeChanged', function() {
        this.trigger('timeupdate');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerPositionChanged', function() {
        this.trigger('progress');
    });
    Vlc.registerEvent(this.getApi(), 'MediaPlayerLengthChanged', function() {
        this.trigger('durationchange');
    });

    return this.el_;
  }

  /**
   * Destroy the component's DOM element
   *
   * @return {}
   * @method dispose
   */
  dispose() {
      if (this.el_) {
          this.el_.parentNode.removeChild(this.el_);
      }

      Tech.prototype.dispose.call(this);
  }


  /**
   * Get/set video
   *
   * @param {Object=} src Source object
   * @return {Object}
   * @method src
   */
  src(src) {
    if (src === undefined) {
      return this.currentSrc();
    }

    // Setting src through `src` not `setSrc` will be deprecated
    return this.setSrc(src);
  }

  /**
   * Set video
   *
   * @param {Object=} src Source object
   * @deprecated
   * @method setSrc
   */
  setSrc(src) {
    // Make sure source URL is absolute.
    src = Url.getAbsoluteURL(src);
    this.getApi().playlist.items.clear();
    this.getApi().playlist.add(src);

    // Currently the SWF doesn't autoplay if you load a source later.
    // e.g. Load player w/ no source, wait 2s, set src.
    if (this.autoplay()) {
      var tech = this;
      this.setTimeout(function(){ tech.play(); }, 0);
    }
  }

  currentSrc() {
      if (this.currentSource_) {
          return this.currentSource_.src;
      }
      else {
          return this.getApi().playlist.items[this.getApi().playlist.currentItem];
      }
  }

  /**
   * Returns true if the tech is currently seeking.
   * @return {boolean} true if seeking
   */
  seeking() {
    return this.lastSeekTarget_ !== undefined;
  }


  /**
   * Load media into player
   *
   * @method load
   */
  load() {
    // this.el_.vjs_load();
  }

  /**
   * Get poster
   *
   * @method poster
   */
  poster() {
    this.el_.vjs_getProperty('poster');
  }

  setPoster(){
    // poster images are not handled by the VLC tech so make this a no-op
  }

  play() {
    this.getApi().playlist.play();
  }

  ended() {
     var state = this.getApi().input.state;
    return (state === 6 /* ENDED */ || state === 7 /* ERROR */);
  }

  pause() {
      this.getApi().playlist.pause();
  }

  paused() {
      var state = this.getApi().input.state;
      return (state === 4 /* PAUSED */ || state === 6 /* ENDED */);
  }

  currentTime() {
      return (this.getApi().input.time / 1000);
  }

  setCurrentTime (seconds) {
    this.getApi().input.time = (seconds * 1000);
  }

  duration() {
      return (this.getApi().input.length / 1000);
  }

  volume() {
      return this.getApi().audio.volume / 100;
  }

  setVolume (percentAsDecimal) {
      if (percentAsDecimal) {
          this.getApi().audio.volume = percentAsDecimal * 100;
      }
  }

  muted() {
      return this.getApi().audio.mute;
  }

  setMuted(muted) {
      this.getApi().audio.mute.mute = muted;
  }

  /**
   * Determine if can seek in media
   *
   * @return {TimeRangeObject}
   * @method seekable
   */
  seekable() {
    const duration = this.duration();
    if (duration === 0) {
      return createTimeRange();
    }
    return createTimeRange(0, duration);
  }

  /**
   * Get buffered time range
   *
   * @return {TimeRangeObject}
   * @method buffered
   */
  buffered() {
      // Not supported
      return [];
  }

  /**
   * Get fullscreen support -
   * Vlc does not allow fullscreen through javascript
   * so always returns false
   *
   * @return {Boolean} false
   * @method supportsFullScreen
   */
  supportsFullScreen() {
    return true; // Vlc does not allow fullscreen through javascript
  }

  /**
   * Request to enter fullscreen
   * Vlc does not allow fullscreen through javascript
   * so always returns false
   *
   * @return {Boolean} false
   * @method enterFullScreen
   */
  enterFullScreen() {
   this.getApi().video.fullscreen = true;
   this.player_.trigger('fullscreenchange');
  }

  exitFullScreen(){
      this.getApi().video.fullscreen = false;
      this.player_.trigger('fullscreenchange');
  }

  getApi() {
      return this.el_;
  }

}


// Create setters and getters for attributes
const _api = Vlc.prototype;
const _readWrite = 'rtmpConnection,rtmpStream,preload,defaultPlaybackRate,playbackRate,autoplay,loop,mediaGroup,controller,controls,volume,muted,defaultMuted'.split(',');
const _readOnly = 'networkState,readyState,initialTime,duration,startOffsetTime,paused,ended,videoWidth,videoHeight'.split(',');

function _createSetter(attr){
  var attrUpper = attr.charAt(0).toUpperCase() + attr.slice(1);
  _api['set'+attrUpper] = function(val){ return this.el_.vjs_setProperty(attr, val); };
}
function _createGetter(attr) {
  _api[attr] = function(){ return this.el_.vjs_getProperty(attr); };
}

// Create getter and setters for all read/write attributes
for (let i = 0; i < _readWrite.length; i++) {
  _createGetter(_readWrite[i]);
  _createSetter(_readWrite[i]);
}

// Create getters for read-only attributes
for (let i = 0; i < _readOnly.length; i++) {
  _createGetter(_readOnly[i]);
}

/* Vlc Support Testing -------------------------------------------------------- */

Vlc.isSupported = function () {
    var vlc;

    if(window.ActiveXObject) {
        try {
            vlc = new window.ActiveXObject('VideoLAN.VLCPlugin.2');
        } catch(e) {}
    }
    else if(navigator.plugins && navigator.mimeTypes.length > 0) {
        var name = 'VLC';
        if (navigator.plugins && (navigator.plugins.length > 0)) {
            for(var i=0;i<navigator.plugins.length;++i) {
                if (navigator.plugins[i].name.indexOf(name) !== -1) {
                    vlc = navigator.plugins[i];
                    return true;
                }
            }
        }
    }
    // else {
    //     obj = document.getElementById('test_vlc');
    //     if(obj === null){
    //       obj =  document.createElement('embed');
    //       obj.setAttribute('style', 'width:0px; height:0px;');
    //       obj.setAttribute('type', 'application/x-vlc-plugin');
    //       obj.setAttribute('id','test_vlc');
    //       document.body.appendChild(obj);
    //     }
    //     if(typeof obj.playlist !== 'undefined'){
    //         obj.parentNode.removeChild(obj);
    //         return true;
    //     }
    // }

    if (vlc) {
        return true;
    }
    return false;
};

// Add Source Handler pattern functions to this tech
Tech.withSourceHandlers(Vlc);

/*
 * The default native source handler.
 * This simply passes the source to the video element. Nothing fancy.
 *
 * @param  {Object} source   The source object
 * @param  {Vlc} tech  The instance of the Vlc tech
 */
Vlc.nativeSourceHandler = {};

/**
 * Check if Vlc can play the given videotype
 * @param  {String} type    The mimetype to check
 * @return {String}         'probably', 'maybe', or '' (empty string)
 */
Vlc.nativeSourceHandler.canPlayType = function(type){
  if (type in Vlc.formats) {
    return 'maybe';
  }

  return '';
};

/*
 * Check Vlc can handle the source natively
 *
 * @param  {Object} source  The source object
 * @return {String}         'probably', 'maybe', or '' (empty string)
 */
Vlc.nativeSourceHandler.canHandleSource = function(source){
  var type;

  function guessMimeType(src) {
    var ext = Url.getFileExtension(src);
    if (ext) {
      return `video/${ext}`;
    }
    return '';
  }

  if (!source.type) {
    type = guessMimeType(source.src);
  } else {
    // Strip code information from the type because we don't get that specific
    type = source.type.replace(/;.*/, '').toLowerCase();
  }

  return Vlc.nativeSourceHandler.canPlayType(type);
};

/*
 * Pass the source to the vlc object
 * Adaptive source handlers will have more complicated workflows before passing
 * video data to the video element
 *
 * @param  {Object} source   The source object
 * @param  {Vlc}  tech     The instance of the Vlc tech
 * @param  {Object} options  The options to pass to the source
 */
Vlc.nativeSourceHandler.handleSource = function(source, tech, options){
  tech.setSrc(source.src);
};

/*
 * Clean up the source handler when disposing the player or switching sources..
 * (no cleanup is needed when supporting the format natively)
 */
Vlc.nativeSourceHandler.dispose = function(){};

// Register the native source handler
Vlc.registerSourceHandler(Vlc.nativeSourceHandler);

Vlc.formats = {
  'video/flv': 'FLV',
  'video/x-flv': 'FLV',
  'video/mp4': 'MP4',
  'video/webm': 'MKV',
  'video/m4v': 'MP4'
};

Vlc.registerEvent = function(vlc, event, handler) {
    if (vlc) {
        if (vlc.attachEvent) {
            // Microsoft
            vlc.attachEvent (event, handler);
        } else if (vlc.addEventListener) {
            // Mozilla: DOM level 2
            vlc.addEventListener(event, handler, false);
        } else {
            // DOM level 0
            vlc['on' + event] = handler;
        }
    }
};

Vlc.unregisterEvent = function(vlc, event, handler) {
    if (vlc) {
        if (vlc.detachEvent) {
            // Microsoft
            vlc.detachEvent (event, handler);
        } else if (vlc.removeEventListener) {
            // Mozilla: DOM level 2
            vlc.removeEventListener(event, handler, false);
        } else {
            // DOM level 0
            vlc['on' + event] = null;
        }
    }
};

// Vlc embedding method. Only used in non-iframe mode
Vlc.embed = function(vlcVars, params, attributes){
  const code = Vlc.getEmbedCode(vlcVars, params, attributes);

  // Get element by embedding code and retrieving created element
  const obj = Dom.createEl('div', { innerHTML: code }).childNodes[0];

  return obj;
};

Vlc.getEmbedCode = function(vlcVars, params, attributes){
  const objTag = '<object classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921" codebase="http://download.videolan.org/pub/videolan/vlc/last/win32/axvlc.cab" id="vlc" ';
  let vlcVarsString = '';
  let paramsString = '';
  let embedString = '';
  let attrsString = '';

  // Convert vlc vars to string
  if (vlcVars) {
    Object.getOwnPropertyNames(vlcVars).forEach(function(key){
      vlcVarsString += `${key}=${vlcVars[key]}&amp;`;
    });
  }

  // Add swf, vlcVars, and other default params
  params = assign({
    'autoplay': 'false' // All should be default, but having security issues.
  }, params);

  // Create param tags string
  Object.getOwnPropertyNames(params).forEach(function(key){
    paramsString += `<param name="${key}" value="${params[key]}" />`;
  });

  attributes = assign({
    // Default to 100% width/height
    'width': '100%',
    'height': '100%'

  }, attributes);

  // Create Attributes string
  Object.getOwnPropertyNames(attributes).forEach(function(key){
    attrsString += `${key}="${attributes[key]}" `;
  });

  embedString = '<embed type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" version="VideoLAN.VLCPlugin.2" width=640 height=480 target="http://dl5.watchfilm.ir/Foreign.Movie/Action/Deadpool.2016/Deadpool.2016.1080p.HDRip.mkv" />';

  return `${objTag}>${paramsString}${embedString}</object>`;
};

Component.registerComponent('Vlc', Vlc);
Tech.registerTech('Vlc', Vlc);
export default Vlc;
